package tb.sockets.client;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.io.PrintWriter;
import tb.sockets.client.MainFrame;

public class Konsola {
	Socket sock;
	BufferedReader is;
	InputStream in;
	PrintWriter pw;
	OutputStream os;
	static boolean kolej = true;
	int input;
	int ruchy = 0;

	public Konsola(String host, int port) throws Exception {
		String IP = host;
		int portnr = port;
		try {
			sock = new Socket(IP, portnr);
			os = sock.getOutputStream();
			pw = new PrintWriter(os, true);
			in = sock.getInputStream();
			is = new BufferedReader(new InputStreamReader(in));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Rozgrywka() throws Exception {
		MainFrame.Kolejka(kolej);
		while (true) {
			pw.flush();
			if (kolej == true) {
				if (MainFrame.wybor != 10 && MainFrame.btn[MainFrame.wybor].getText() == "") {
					MainFrame.btn[MainFrame.wybor].setText("X");
					pw.write(MainFrame.wybor);
					ruchy++;
					MainFrame.wybor = 10;
					pw.flush();
					if (ruchy >= 5)
						if (MainFrame.Wincheck("X") == true) {
							MainFrame.lblkolej.setText("        Wygrales!");
							break;
						}
					if (ruchy == 9) {
						MainFrame.lblkolej.setText("          Remis");
						break;
					}
					kolej = false;
					MainFrame.Kolejka(kolej);
				}
			}
			if (kolej == false) {
				input = in.read();
				if (MainFrame.btn[input].getText() == "") {
					MainFrame.btn[input].setText("O");
					ruchy++;
					pw.flush();
					if (ruchy >= 5)
						if (MainFrame.Wincheck("O") == true) {
							MainFrame.lblkolej.setText("  Przeciwnik Wygral");
							break;
						}
					if (ruchy == 9) {
						MainFrame.lblkolej.setText("          Remis");
						break;
					}
					kolej = true;
					MainFrame.Kolejka(true);
				}
			}

		}
		pw.close();
		sock.close();
		os.close();
		in.close();
		is.close();
	}
}

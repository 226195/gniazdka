package tb.sockets.client;

import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import tb.sockets.client.kontrolki.KKButton;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.LineBorder;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	static int wybor = 10;
	static KKButton[] btn = new KKButton[9];
	static Konsola kon;
	static MainFrame frame;
	static JLabel lblkolej = new JLabel();

	public static void Kolejka(boolean turn) {
		boolean t = turn;
		if (t == false) {
			lblkolej.setText("   Przeciwnika Kolej");
			lblkolej.setForeground(new Color(255, 255, 255));
			lblkolej.setBackground(new Color(128, 128, 128));
		} else {
			lblkolej.setText("       Twoja Kolej");
			lblkolej.setBackground(new Color(100, 255, 100));
			lblkolej.setForeground(new Color(0, 0, 0));
		}
	}

	public static boolean Wincheck(String KolkoKrzyzyk) {
		int podrzad = 0;
		String Ch = KolkoKrzyzyk;
		for (int i = 0; i < 4; i = i + 3) { // sprawdzanie w poziomie
			podrzad = 0;
			for (int j = i; j <= i + 2; j++) {
				if (btn[j].getText() == Ch)
					podrzad++;
				else
					break;
			}
			if (podrzad == 3)
				return true;
		}
		for (int i = 0; i <= 2; i++) { // sprawdzanie w pionie
			podrzad = 0;
			for (int j = i; j <= i + 6; j = j + 3) {
				if (btn[j].getText() == Ch)
					podrzad++;
				else
					break;
			}
			if (podrzad == 3)
				return true;
		}
		if ((btn[0].getText() == Ch && btn[4].getText() == Ch && btn[8].getText() == Ch)
				|| (btn[2].getText() == Ch && btn[4].getText() == Ch && btn[6].getText() == Ch))
			return true; // sprawdzenie po skosie
		return false;
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		new Thread() {
			public void run() {
				try {
					kon = new Konsola("localhost", 6660);
					kon.Rozgrywka();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 440, 580);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_1.setBounds(10, 10, 400, 400);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(3, 3, 5, 5));
		Font font = new Font("Arial", Font.BOLD, 90);
		Font lblfont = new Font("Arial", Font.BOLD, 40);
		lblkolej = new JLabel("       Twoja Kolej");
		lblkolej.setBackground(new Color(100, 255, 100));
		lblkolej.setForeground(new Color(0, 0, 0));
		lblkolej.setFont(lblfont);
		lblkolej.setOpaque(true);
		lblkolej.setBounds(10, 420, 400, 100);
		contentPane.add(lblkolej);

		for (int i = 0; i < 9; i++) {
			btn[i] = new KKButton();
			btn[i].setFont(font);
			panel_1.add(btn[i]);
			final Integer inneri = new Integer(i);
			btn[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					wybor = inneri;
				}

			});
		}

	}
}

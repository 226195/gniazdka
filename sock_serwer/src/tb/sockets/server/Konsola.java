package tb.sockets.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import tb.sockets.server.MainFrame;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Konsola {

	ServerSocket sSock;
	Socket sock;
	InputStream in;
	OutputStream os;
	BufferedReader is;
	PrintWriter pw;
	static boolean kolej = false;
	int input;
	int ruchy = 0;

	public Konsola() throws Exception {
		sSock = new ServerSocket(6660);
		sock = sSock.accept();
		os = sock.getOutputStream();
		pw = new PrintWriter(os, true);
		in = sock.getInputStream();
		is = new BufferedReader(new InputStreamReader(in));
	}

	public void Rozgrywka() throws Exception {
		MainFrame.Kolejka(kolej);
		while (true) {
			pw.flush();
			if (kolej == true) {
				if (MainFrame.wybor != 10 && MainFrame.btn[MainFrame.wybor].getText() == "") {
					MainFrame.btn[MainFrame.wybor].setText("O");
					pw.write(MainFrame.wybor);
					ruchy++;
					MainFrame.wybor = 10;
					pw.flush();
					if (ruchy >= 5)
						if (MainFrame.Wincheck("O") == true) {
							MainFrame.lblkolej.setText("        Wygrales!");
							break;
						}
					if (ruchy == 9) {
						MainFrame.lblkolej.setText("          Remis");
						break;
					}
					kolej = false;
					MainFrame.Kolejka(kolej);
				}
			}

			if (kolej == false) {
				input = in.read();
				if (MainFrame.btn[input].getText() == "") {
					MainFrame.btn[input].setText("X");
					ruchy++;
					pw.flush();
					if (ruchy >= 5)
						if (MainFrame.Wincheck("X") == true) {
							MainFrame.lblkolej.setText("  Przeciwnik Wygral");
							break;
						}
					if (ruchy == 9) {
						MainFrame.lblkolej.setText("          Remis");
						break;
					}
					kolej = true;
					MainFrame.Kolejka(true);
				}
			}

		}
		pw.close();
		sock.close();
		os.close();
		in.close();
		is.close();
	}
}
